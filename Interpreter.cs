using System;
using System.Collections.Generic;

namespace StackLang {
    public class Interpreter {
        //Stack<int> intStack;
        //Stack<string> stringStack;
        //Stack<string> variableStack;
        Stack<Data> stack;
        Stack<string> procedureStack;
        //Dictionary<string, int> integers;
        Dictionary<string, Data> variables;

        public Interpreter() {
            //intStack = new Stack<int>();
            //stringStack = new Stack<String>();
            //variableStack = new Stack<string>();
            stack = new Stack<Data>();
            procedureStack = new Stack<String>();
            //integers = new Dictionary<string, int>();
            variables = new Dictionary<string, Data>();
        }

        public void Interpret(string line) {
            string lexeme = "";
            for (int i = 0; i < line.Length; i++) {
                if (Char.IsDigit(line[i])) {
                    while(i < line.Length && line[i] != ' ') {
                        lexeme += line[i];
                        i++;
                    }
                    //intStack.Push(Convert.ToInt32(lexeme));
                    stack.Push(new Data(0, lexeme));
                    lexeme = "";
                }
                else if (Char.IsLetter(line[i])) {
                    while (i < line.Length && line[i] != ' ') {
                        lexeme += line[i];
                        i++;
                    }
                    if (!execute(lexeme)) {
                        if (variables.ContainsKey(lexeme)) {
                            //intStack.Push(integers[lexeme]);
                            stack.Push(variables[lexeme]);
                        }
                    }
                    lexeme = "";
                }
                else if (line[i] == '$') {
                    i++;
                    while (i < line.Length && line[i] != ' ') {
                        lexeme += line[i];
                        i++;
                    }
                    //variableStack.Push(lexeme);
                    stack.Push(new Data(4, lexeme));
                    lexeme = "";
                }
                else if (line[i] == '"') {
                    i++;
                    while (true) {
                        //Console.WriteLine(line[i]);
                        if (line[i] != '\\' && i - 1 >= 0 && line[i - 1] != '\\')
                            lexeme += line[i];
                        i++;
                        if (i >= line.Length) {
                            Console.WriteLine("Error: invalid string");
                            lexeme = "";
                            break;
                        }
                        if (line[i] == '"') {
                            //stringStack.Push(lexeme);
                            if (i - 1 >= 0 &&  i - 1 >= 0 && line[i - 1] == '\\') {
                                
                            }
                            else {
                                stack.Push(new Data(1, lexeme));
                                lexeme = "";
                                break;
                            }
                        }
                    }
                }
                else if (line[i] == '{') {
                    Stack<Char> braceStack = new Stack<Char>();
                    i++;
                    while (true) {
                        lexeme += line[i];
                        i++;
                        if (line[i] == '{') {
                            braceStack.Push('{');
                        }
                        if (i >= line.Length) {
                            Console.WriteLine("Error: invalid procedure");
                            lexeme = "";
                            break;
                        }
                        if (line[i] == '}') {
                            if (braceStack.Count == 0) {
                                procedureStack.Push(lexeme);
                                lexeme = "";
                                break;
                            }
                            else {
                                braceStack.Pop();
                            }
                        }
                    }
                }
                else if (line[i] == '+') {
                    Data x = stack.Pop();
                    Data y = stack.Pop();
                    if (x.Type != 0) {
                        Console.WriteLine("Error: " + x.Content + " is not an integer.");
                    }
                    else if (y.Type != 0) {
                        Console.WriteLine("Error: " + y.Content + " is not an integer.");
                    }
                    else {
                        int xInt = Convert.ToInt32(x.Content);
                        int yInt = Convert.ToInt32(y.Content);
                        stack.Push(new Data(0, Convert.ToString(xInt + yInt)));
                    }
                }
                else if (line[i] == '-') {
                    Data x = stack.Pop();
                    Data y = stack.Pop();
                    if (x.Type != 0) {
                        Console.WriteLine("Error: " + x.Content + " is not an integer.");
                    }
                    else if (y.Type != 0) {
                        Console.WriteLine("Error: " + y.Content + " is not an integer.");
                    }
                    else {
                        int xInt = Convert.ToInt32(x.Content);
                        int yInt = Convert.ToInt32(y.Content);
                        stack.Push(new Data(0, Convert.ToString(yInt - xInt)));
                    }
                }
                else if (line[i] == '*') {
                    Data x = stack.Pop();
                    Data y = stack.Pop();
                    if (x.Type != 0) {
                        Console.WriteLine("Error: " + x.Content + " is not an integer.");
                    }
                    else if (y.Type != 0) {
                        Console.WriteLine("Error: " + y.Content + " is not an integer.");
                    }
                    else {
                        int xInt = Convert.ToInt32(x.Content);
                        int yInt = Convert.ToInt32(y.Content);
                        stack.Push(new Data(0, Convert.ToString(xInt * yInt)));
                    }
                }
                else if (line[i] == '/') {
                    Data x = stack.Pop();
                    Data y = stack.Pop();
                    if (x.Type != 0) {
                        Console.WriteLine("Error: " + x.Content + " is not an integer.");
                    }
                    else if (y.Type != 0) {
                        Console.WriteLine("Error: " + y.Content + " is not an integer.");
                    }
                    else {
                        int xInt = Convert.ToInt32(x.Content);
                        int yInt = Convert.ToInt32(y.Content);
                        stack.Push(new Data(0, Convert.ToString(yInt / xInt)));
                    }
                }
                else if (line[i] == '%') {
                    Data x = stack.Pop();
                    Data y = stack.Pop();
                    if (x.Type != 0) {
                        Console.WriteLine("Error: " + x.Content + " is not an integer.");
                    }
                    else if (y.Type != 0) {
                        Console.WriteLine("Error: " + y.Content + " is not an integer.");
                    }
                    else {
                        int xInt = Convert.ToInt32(x.Content);
                        int yInt = Convert.ToInt32(y.Content);
                        stack.Push(new Data(0, Convert.ToString(yInt % xInt)));
                    }
                }
                else if (line[i] == '^') {
                    Data x = stack.Pop();
                    Data y = stack.Pop();
                    if (x.Type != 0) {
                        Console.WriteLine("Error: " + x.Content + " is not an integer.");
                    }
                    else if (y.Type != 0) {
                        Console.WriteLine("Error: " + y.Content + " is not an integer.");
                    }
                    else {
                        int xInt = Convert.ToInt32(x.Content);
                        int yInt = Convert.ToInt32(y.Content);
                        stack.Push(new Data(0, Convert.ToString(Math.Pow(yInt, xInt))));
                    }
                }
                else if (line[i] == '=') {
                    if (i + 1 < line.Length && line[i + 1] == '=') {
                        i++;
                        Data x = stack.Pop();
                        Data y = stack.Pop();
                        if (x.Type == y.Type && x.Content.Equals(y.Content)) {
                            stack.Push(new Data(3, "1"));
                        }
                        else {
                            stack.Push(new Data(3, "0"));
                        }
                    }
                    else {
                        Data data = stack.Pop();
                        string id = stack.Pop().Content;
                        if (variables.ContainsKey(id)) {
                            variables[id] = data;
                        }
                        else {
                            variables.Add(id, data);
                        }
                    }
                }
                else if (line[i] == '>') {
                    Data x = stack.Pop();
                    Data y = stack.Pop();
                    if (x.Type == 0 && y.Type == 0 && Convert.ToInt32(y.Content) > Convert.ToInt32(x.Content)) {
                        stack.Push(new Data(3, "1"));
                    }
                    else {
                        stack.Push(new Data(3, "0"));
                    }
                }
                else if (line[i] == '<') {
                    Data x = stack.Pop();
                    Data y = stack.Pop();
                    if (x.Type == 0 && y.Type == 0 && Convert.ToInt32(y.Content) < Convert.ToInt32(x.Content)) {
                        stack.Push(new Data(3, "1"));
                    }
                    else {
                        stack.Push(new Data(3, "0"));
                    }
                }
            }
            
            /*
            string[] tokens = line.Split(" ");
            foreach (string token in tokens) {
                int i = 0;
                if (int.TryParse(token, out i)) {
                    stack.Push(Convert.ToInt32(token));
                }
                else {
                    int x;
                    int y;
                    switch (token) {
                        case "+":
                            x = stack.Pop();
                            y = stack.Pop();
                            stack.Push(x + y);
                            break;
                        case "-":
                            x = stack.Pop();
                            y = stack.Pop();
                            stack.Push(y - x);
                            break;
                        case "*":
                            x = stack.Pop();
                            y = stack.Pop();
                            stack.Push(x * y);
                            break;
                        case "/":
                            x = stack.Pop();
                            y = stack.Pop();
                            stack.Push(y / x);
                            break;
                        case "%":
                            x = stack.Pop();
                            y = stack.Pop();
                            stack.Push(x % y);
                            break;
                        case "^":
                            x = stack.Pop();
                            y = stack.Pop();
                            stack.Push(x ^ y);
                            break;
                        case "print":
                            Console.WriteLine(Convert.ToString(stack.Pop()));
                            break;
                        default:
                            Console.WriteLine("ERROR!!!!! " + token + " is not a valid keyword.");
                            break;
                    }
                }
            }
            */
        }

        bool execute(string statement) {
            //int x;
            //int y;
            string procedure;
            string elseProcedure;
            string condition;
            switch (statement) {
                case "intToString":
                    stack.Push(new Data(1, stack.Pop().Content));
                    return true;
                    break;
                case "stringToInt":
                    stack.Push(new Data(0, stack.Pop().Content));
                    return true;
                    break;
                case "true":
                    stack.Push(new Data(3, "1"));
                    return true;
                    break;
                case "false":
                    stack.Push(new Data(3, "0"));
                    return true;
                    break;
                case "print":
                    Console.Write(stack.Pop().Content);
                    return true;
                    break;
                case "println":
                    Console.WriteLine(stack.Pop().Content);
                    return true;
                    break;
                case "input":
                    string x;
                    x = Console.ReadLine();
                    stack.Push(new Data(1, x));
                    return true;
                    break;
                case "cat":
                    string a = stack.Pop().Content;
                    string b = stack.Pop().Content;
                    stack.Push(new Data(1, b + a));
                    return true;
                    break;
                case "if":
                    procedure = procedureStack.Pop();
                    condition = stack.Pop().Content;
                    if (Convert.ToInt32(condition) > 0) {
                        Interpret(procedure);
                    }
                    break;
                case "ifelse":
                    elseProcedure = procedureStack.Pop();
                    procedure = procedureStack.Pop();
                    condition = stack.Pop().Content;
                    if (Convert.ToInt32(condition) > 0) {
                        Interpret(procedure);
                    }
                    else {
                        Interpret(elseProcedure);
                    }
                    break;
                case "REM":
                    break;
            }
            return false;
        }
    }
}