using System;
namespace StackLang {
    public struct Data {
        public int Type; // 0 = int; 1 = string; 3 = bool; 4 = variable
        public string Content;

        public Data(int type, string content) {
            this.Type = type;
            this.Content = content;
        }
    }
}