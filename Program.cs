﻿using System;

namespace StackLang
{
    class Program
    {
        static void Main(string[] args)
        {
            Interpreter interpreter = new Interpreter();
            while (true) {
                Console.Write("> ");
                string line = Console.ReadLine();
                interpreter.Interpret(line);
            }
        }
    }
}
